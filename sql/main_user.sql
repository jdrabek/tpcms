create table user
(
    id        INTEGER      not null
        primary key autoincrement,
    email     VARCHAR(180) not null,
    roles     CLOB         not null,
    password  VARCHAR(255) not null,
    nickname  VARCHAR(255) default NULL,
    firstname VARCHAR(255) not null,
    lastname  VARCHAR(255) not null
);

create unique index UNIQ_8D93D649E7927C74
    on user (email);

INSERT INTO user (id, email, roles, password, nickname, firstname, lastname) VALUES (1, 'jd@d3v.eu', '["ROLE_ADMIN"]', '$argon2id$v=19$m=65536,t=4,p=1$BQG+jovPcunctc30xG5PxQ$TiGbx451NKdo+g9vLtfkMy4KjASKSOcnNxjij4gTX1s', 'jd', 'Jakub', 'Drabek');