create table post
(
    id         INTEGER  not null
        primary key autoincrement,
    user_id    INTEGER  not null
        constraint FK_5A8A6C8DA76ED395
            references user,
    title      CLOB     not null,
    content    CLOB     not null,
    created_at DATETIME not null
);

create index IDX_5A8A6C8DA76ED395
    on post (user_id);

INSERT INTO post (id, user_id, title, content, created_at) VALUES (2, 1, 'asd', 'ddddddd', '2020-08-26 22:42:14');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (3, 1, 'asd', 'ddddddd', '2020-08-26 22:44:11');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (4, 1, 'asd', 'ddddddd', '2020-08-26 22:46:16');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (5, 1, 'asd', 'ddddddd', '2020-08-26 22:54:00');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (6, 1, 'asd', 'ddddddd', '2020-08-26 23:19:19');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (7, 1, 'asd', 'ddddddd', '2020-08-26 23:25:01');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (8, 1, 'asd', 'ddddddd', '2020-08-26 23:46:44');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (9, 1, 'asd', 'ddddddd', '2020-08-26 23:48:56');
INSERT INTO post (id, user_id, title, content, created_at) VALUES (10, 1, 'asd', 'ddddddd', '2020-08-26 23:51:30');