create table author
(
    id        INTEGER      not null
        primary key autoincrement,
    nickname  VARCHAR(255) default NULL,
    firstname VARCHAR(255) not null,
    email     VARCHAR(255) not null,
    lastname  VARCHAR(255) not null,
    avatar    VARCHAR(255) default NULL
);

INSERT INTO author (id, nickname, firstname, email, lastname, avatar) VALUES (1, 'asd', 'asd', 'asd@sad.pl', 'ads', '1');