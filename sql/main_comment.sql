create table comment
(
    id         INTEGER  not null
        primary key autoincrement,
    post_id    INTEGER  not null
        constraint FK_9474526C4B89032C
            references post,
    user_id    INTEGER  not null
        constraint FK_9474526CA76ED395
            references user,
    content    CLOB     not null,
    created_at DATETIME not null
);

create index IDX_9474526C4B89032C
    on comment (post_id);

create index IDX_9474526CA76ED395
    on comment (user_id);

