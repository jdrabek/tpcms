<?php

require dirname(__DIR__).'/config/bootstrap.php';

use App\Helper\Response;
use App\Kernel;
use Symfony\Component\HttpFoundation\Request;


$kernel = new Kernel('dev', true);
$request = Request::createFromGlobals();
try {
    $response = $kernel->handle($request);
} catch (Exception $e) {
    $response = Response::error([
        'message' => $e->getMessage(),
        'line' => $e->getLine(),
        'file' => $e->getFile()
    ]);
}
$response->send();
$kernel->terminate($request, $response);
