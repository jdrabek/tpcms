<?php


namespace App\Controller;


use http\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends AbstractController
{

    protected function getInputData(Request $request, array $requiredFields) : \stdClass {
        $json = json_decode($request->getContent());

        if(!$json){
            throw new InvalidArgumentException('invalid input data');
        }
        if($requiredFields){
            foreach ($requiredFields as $field){
                if(!property_exists($json, $field)){
                    throw new InvalidArgumentException("field $field must be providen");
                }
            }
        }
        return $json;
    }
}