<?php

namespace App\Controller;

use App\Helper\Response;
use App\Service\PostService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class PostController extends BaseController
{
    private PostService $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }


    public function addPost(Request $request): JsonResponse
    {
        try {
            $json = $this->getInputData($request, ['title', 'content']);
            $newPostId = $this->postService->save($json);

        } catch (\Exception $e){
            return Response::error($e->getMessage());
        }

        return Response::success([
            'id' => $newPostId
        ]);
    }

    public function showPost(int $id) : JsonResponse
    {
       $post = $this->postService->getPostContentById($id);

        return Response::success([
            $post
        ]);
    }

    public function listPosts() : JsonResponse
    {
        return Response::success([
            $this->postService->getAll()
        ]);
    }

    public function deletePost(int $id){
        try {
            $this->postService->delete($id);
        } catch (\Exception $e){
            return Response::error($e->getMessage());
        }

        return Response::success([
            'status' => 'OK'
        ]);
    }
}