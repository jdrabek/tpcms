<?php


namespace App\Helper;


use Symfony\Component\HttpFoundation\JsonResponse;

class Response
{
    public static function success($message)
    {
        return new JsonResponse($message,200);
    }

    public static function error($message)
    {
        return new JsonResponse($message,500);
    }
}