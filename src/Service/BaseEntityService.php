<?php


namespace App\Service;


use App\Entity\BaseEntity;
use Doctrine\ORM\EntityManagerInterface;

class BaseEntityService
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function saveEntity(BaseEntity $entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

}