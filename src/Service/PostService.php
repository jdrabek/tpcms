<?php


namespace App\Service;


use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class PostService extends BaseEntityService
{

    const REPOSITORY_NAME = 'App:Post';

    public function save(\stdClass $data): int
    {
        $post = new Post();
        $post->setTitle($data->title);
        $post->setContent($data->content);

        // @todo: mock zanim nie powstanie obsluga logowania
        $post->setUser($this->entityManager->getRepository('App:User')->find(1));

        $this->saveEntity($post);
        return $post->getId();
    }

    // @todo sprawdzić czy post należy do uzytkownika badz uzytkownik jest adminem
    public function delete(int $id): void
    {
        $post = $this->entityManager->getRepository('App:Post')->find($id);
        if (!$post) {
            throw new NotFoundHttpException('post not found');
        }

        $this->entityManager->remove($post);
        $this->entityManager->flush();
    }


    public function getPostContentById(int $id): array
    {
        /* @var App\Entity\Post */
        $post = $this->entityManager->getRepository(self::REPOSITORY_NAME)->find($id);
        if (!$post) {
            throw new NotFoundHttpException('post not found');
        }
        return [
            'id' => $post->getId(),
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'author_id' => $post->getUser()->getId()
        ];

    }

    public function getAll(): array
    {
        $posts = $this->entityManager->getRepository(self::REPOSITORY_NAME)->findAll();
        if (empty($posts)) {
            throw new NotFoundHttpException('post not found');
        }
        return array_map(function ($post) {
            return [
                'id' => $post->getId(),
                'title' => $post->getTitle(),
                'content' => $post->getContent(),
                'author_id' => $post->getUser()->getId()
            ];
        }, $posts);


    }

}